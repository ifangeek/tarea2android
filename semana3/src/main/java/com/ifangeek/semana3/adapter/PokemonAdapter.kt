package com.ifangeek.semana3.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ifangeek.semana3.R
import com.ifangeek.semana3.model.Pokemon
import com.squareup.picasso.Picasso

class PokemonAdapter(
    val listPokemons: MutableList<Pokemon>,
    val itemCallbackPokemon: (item: Pokemon) -> Unit
) :
    RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        //item.xml - Inflar la vista en el adapter
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)

        // luego de inflar la vista se la paso al ViewHolder por medio de su constructor para poder obtener las referencias de los id's del item que infle
        return PokemonViewHolder(view)

    }

    override fun getItemCount(): Int = listPokemons.size

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        //pokemons 9  elementos 3
        //position = 1
        val pokemon = listPokemons[position]

        holder.tvPokemon.text = pokemon.nombre
        Picasso.get().load(pokemon.url).into(holder.imgPokemon)

        holder.itemView.setOnClickListener {
            Log.i(PokemonAdapter::class.java.simpleName, pokemon.nombre)
            itemCallbackPokemon(pokemon)
        }


    }

    //ViewHolder
    class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPokemon: ImageView = itemView.findViewById(R.id.ivPokemon)
        var tvPokemon: TextView = itemView.findViewById(R.id.tvPokemonName)
    }
}