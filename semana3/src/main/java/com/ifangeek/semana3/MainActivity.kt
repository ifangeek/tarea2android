package com.ifangeek.semana3

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ifangeek.semana3.adapter.PokemonAdapter
import com.ifangeek.semana3.model.Pokemon
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var pokemons = mutableListOf<Pokemon>() // Lista mutable - tamaño

    //val adapter : PokemonAdapter by lazy { PokemonAdapter() }
    //var adapter: PokemonAdapter? = null
    lateinit var pokemonAdapter: PokemonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadData()
        configurarAdapter()
    }

    private fun configurarAdapter() {
        pokemonAdapter = PokemonAdapter(pokemons) { pokemon ->
            Log.i(MainActivity::class.java.simpleName, pokemon.nombre)
            val bundle = Bundle().apply {
                putSerializable("key_string", pokemon)
            }

            val intent = Intent(this, DetailActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        } //Size:9 <---> position: 0--8
        rvPokedex.apply {
            adapter = pokemonAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun loadData() {
        pokemons.add(
            Pokemon(
                id = 1,
                nombre = "Bulbasaur",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
            )
        )

        pokemons.add(
            Pokemon(
                id = 2,
                nombre = "Ivysaur",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"
            )
        )

        pokemons.add(
            Pokemon(
                id = 3,
                nombre = "Venusaur",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 4,
                nombre = "Charmander",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 5,
                nombre = "Charmeleon",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/5.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 6,
                nombre = "Charizard",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 7,
                nombre = "Squirtle",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 8,
                nombre = "Wartortle",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/8.png"
            )
        )
        pokemons.add(
            Pokemon(
                id = 9,
                nombre = "Blastoise",
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png"
            )
        )

    }
}
