package com.ifangeek.semana3

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ifangeek.semana3.model.Pokemon

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val bundle: Bundle? = intent.extras

        bundle?.let {
            val pokemon = it.getSerializable("key_nombre") as Pokemon
            Toast.makeText(this, "El pokemon elegido es ${pokemon.nombre}", Toast.LENGTH_SHORT).show()
        }
    }
}