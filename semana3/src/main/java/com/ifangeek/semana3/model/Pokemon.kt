package com.ifangeek.semana3.model

import java.io.Serializable

data class Pokemon(
    val id:Int,
    val nombre:String,
    val url:String
) : Serializable