package com.ifangeek.semana02

//En kotlin todas las clases son cerradas( significa que no permite heredar)
// se antepone el "open" para que la clase pueda ser heredada
open class SeleccionFutbol(
    var id: Int,
    var nombre: String,
    var apellidos: String,
    var edad:Int) : IFutbolista{

    override fun concentrarse() {
        println("Estoy concentrado")
    }

    override fun viajar() {
        println("Estoy viajando")
    }
}