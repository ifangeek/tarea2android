package com.ifangeek.semana02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val cursos = listOf("Kotlin", "Java")

        //val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,cursos)
        val adapter = ArrayAdapter(this, R.layout.style_spinner, cursos)
        spCursos.adapter = adapter

        spCursos.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                //Mostrar dialogo al seleccionar un item
                createDialog(cursos[position]).show()
                //  Toast.makeText(this@MainActivity, cursos[position], Toast.LENGTH_LONG).show()
            }
        }
    }

    fun createDialog(lenguaje: String): AlertDialog {

        val alertDialog: AlertDialog
        val builder = AlertDialog.Builder(this)
        //Inflar la vista , se declara un view y se infla mediante este


        val view = layoutInflater.inflate(R.layout.dialog_course, null)


        builder.setView(view)
        alertDialog = builder.create()
        val btnSi: Button = view.findViewById(R.id.btnSi)
        val btnNo: Button = view.findViewById(R.id.btnNo)
        val ivLogo: ImageView = view.findViewById(R.id.ivLogo)

        if (lenguaje == "Kotlin") {
            ivLogo.setBackgroundResource(R.drawable.kotlin)
        } else {
            ivLogo.setBackgroundResource(R.drawable.java)
        }


        btnSi.setOnClickListener {
            Toast.makeText(this, "PULSADO", Toast.LENGTH_SHORT).show()
        }

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }

        return alertDialog

    }


    fun listas() {
        //List ->  inmutable
        // posicion empieza en 0 en los listados
        //tamaño desde 1
        val listaDiasSemana: List<String> =
            listOf("Lunes", "Martes", "Miercoles", "Jueves", "Viernes")

        println(listaDiasSemana.size)
        println(listaDiasSemana[2])
        println(listaDiasSemana.first())
        println(listaDiasSemana.last())
        println(listaDiasSemana)

        val filter = listaDiasSemana.filter { dia ->
            dia == "Lunes" || dia == "Viernes"
        }

        println("Uso de Filtro $filter")

        //Listas mutables -> que se les podria agregar mas items
        val mesesDelAnio: MutableList<String> = mutableListOf("Enero", "Febrero", "Marzo", "Abril")

        println(mesesDelAnio)

        mesesDelAnio.add("Mayo")

        println(mesesDelAnio)

        for (meses in mesesDelAnio) {
            println(meses)
        }

        mesesDelAnio.forEach { meses ->
            println("Items forEach con kotlin ->  $meses")
        }
    }
}
