package com.ifangeek.semana02


//si se especifica val -> significa que las propiedades solo pueden ser obtenidas pero no cambiadas ( inmutables )
// si se especifica var -> pueden ser obtenidas y cambiadas ( mutables )
class Persona(
    var nombres: String,
    var apellidos: String,
    var edad:Int
) {
    fun estudiar() {
        println("Estoy estudiando")
    }
}