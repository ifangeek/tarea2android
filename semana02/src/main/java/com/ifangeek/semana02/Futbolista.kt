package com.ifangeek.semana02

class Futbolista(
    id:Int,
    nombre:String,
    apellido:String,
    edad:Int,
    var dorsal: Int
) : SeleccionFutbol(id,nombre,apellido,edad){
    fun entrenar(){
        println("Estoy entrenando")
    }

    override fun concentrarse() {
        //Que es el super? -> Significa que existe una clase Padre , si no deseamos hacer nada de lo que haga el padre y solo queremos realizar una implementacion de 0 comentamos o retiramos el super.
        //super.concentrarse()
        println("Estoy concentrado con el numero de dorsal $dorsal")
    }


}