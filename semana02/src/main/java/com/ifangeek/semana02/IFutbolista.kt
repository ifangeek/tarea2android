package com.ifangeek.semana02

interface IFutbolista {

    fun concentrarse()

    // se agrega las llaves en la funcion si es que queremos que una funcion de la interfaz pueda ser o no implementada
    fun viajar(){}
}