package com.ifangeek.semana02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class POOActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p_o_o)

        //CLASES -> Plantilla donde se pueden definir atributos
        //OBJETO -> Especifico e Irrepetible // Es una instancia de la clase creada previamente
        //HERENCIA -> Es un mecanismo que permite la definicion de una clase a partir de la definicion de otra ya existente

        //Clases empiezan con Mayuscula
        //Bundle = Clase


        val bundleNombre = Bundle() // Crear objeto o Instanciar un objeto

        val diego = Persona("JuanJose","Ledesma",24)

        println("Mi nombre es ${ diego.nombres}, Mi edad es: ${diego.edad}")

        val seleccionFutbol = SeleccionFutbol(1,"Cristian","Mollinedo",25)

        //Si es que llega nulo no se ejecuta, pero si  llega ejecuta el bloque
        seleccionFutbol?.let {
             println(it.edad)
        }.also {
             println(seleccionFutbol.concentrarse())
            println(seleccionFutbol.viajar())
        }

        val futbolista = Futbolista(2,"alex","Ramirez",27,10)

        futbolista?.let {
            println(it.nombre)
        }.also {
            println(futbolista.entrenar())
            println(futbolista.viajar())
            println(futbolista.concentrarse())
        }
    }
}