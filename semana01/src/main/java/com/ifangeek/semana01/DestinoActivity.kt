package com.ifangeek.semana01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundle : Bundle? = intent.extras

        bundle?.let { bundleLibreDeNull ->
            val nombres = bundleLibreDeNull.getString("key_nombres", "Desconocido")
            val edad = bundleLibreDeNull.getString("key_edad", "0")
            val genero = bundleLibreDeNull.getString("key_genero","")


            tvNombresDestino.text = nombres
            tvEdadDestino.text = edad
            tvGeneroDestino.text = genero
        }
        //TODO Verificar los null Safe


        //Live Templates
        //Toast.makeText(this,nombres,Toast.LENGTH_SHORT).show()


    }
}