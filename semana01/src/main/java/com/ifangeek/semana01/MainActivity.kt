package com.ifangeek.semana01

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.ayuda.*
import kotlinx.android.synthetic.main.ayuda.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /*Comentarios

            var edad = edtEdad.toString().toInt()
            var suma = sumar2(2, 4)
            var suma2 = 2.sumar3(4)
            2 mas 5

        */

        btnEnviar.setOnClickListener {
            //1.Validaciones

            var nombres = edtNombres.text.toString()
            var edad = edtEdad.text.toString()


            //1.1. Nombres no esten vacios
            if (nombres.isEmpty()) {
                //mensaje("Debe ingresar sus nombres")
                this.mensaje("Debe ingresar sus nombres")
                return@setOnClickListener
            }

            //1.2. Edad no este vacio


            if (edad.isEmpty()) {
                //mensaje("Debe ingresar su edad")
                this.mensaje("Debe ingresar su edad")
                return@setOnClickListener
            }

            //1.3. Checkbox de terminos y condiciones este activado
            if (!cbTerminos.isChecked) {
                // mensaje("Debe aceptar los terminos y condiciones")
                this.mensaje("Debe aceptar los terminos y condiciones")
                return@setOnClickListener
            }

            /* var genero = ""

             if(rbMasculino.isChecked){
                 genero = "Masculino"
             } else {
                 genero = "Femenino"
             }*/

            var genero = if (rbMasculino.isChecked) "Masculino" else "Femenino"

            //Forma 1 del apply
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres", nombres)
                putString("key_edad", edad)
                putString("key_genero", genero)
            }

            //Forma 2 del apply
            val intent = Intent(this, DestinoActivity::class.java).apply {
                putExtras(bundle)
            }

            startActivity(intent)
        }

        tvAyuda.setOnClickListener {
            val bottomSheet =
                BottomSheetDialog(this, R.style.BottomSheetDialogtheme)

            //Inflar la vista
            val view = LayoutInflater.from(this).inflate(R.layout.ayuda, clAyuda)

            view.tvTelefono.text = "Hola"

            bottomSheet.setContentView(view)
            bottomSheet.show()
        }
    }

    fun sumar(numero1: Int, numero2: Int): Int {
        return numero1 + numero2
    }

    fun sumar2(numero1: Int, numero2: Int) = numero1 + numero2

    fun Int.sumar3(numero2: Int) = this + numero2

    infix fun Int.mas(numero2: Int) = this + numero2

    fun mensaje(mensaje: String): Unit {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()

    }

    fun Context.mensaje2(mensaje: String) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
    }
}
