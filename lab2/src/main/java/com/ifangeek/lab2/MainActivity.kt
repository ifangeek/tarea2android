package com.ifangeek.lab2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {
            val nombres = edtNombres.text.toString()
            val edad = edtEdad.text.toString()
            val vacuna: MutableList<String> = mutableListOf()

            // Validaciones
            if (nombres.isEmpty()) {
                Toast("Debe ingresar el nombre de la mascota")
                return@setOnClickListener
            }

            if (edad.isEmpty()) {
                Toast("Debe ingresar la edad de la mascota")
                return@setOnClickListener
            }
            if (!cbVacunaDistemper.isChecked && !cbVacunaParvovirus.isChecked && !cbVacunaRabia.isChecked) {
                Toast("Debe seleccionar minimo una vacuna")
                return@setOnClickListener
            }

            //La ayuda que me indico el Android Studio decia modificar mi sentencia IF por WHEN
            // lo hice y la verdad se entiende mejor a mi parecer
            val mascota = when {
                rbConejo.isChecked -> {
                    "conejo"

                }
                rbGato.isChecked -> {
                    "gato"
                }
                else -> {
                    "perro"
                }
            }

            if (cbVacunaRabia.isChecked) {
                vacuna.add(cbVacunaRabia.text.toString())
            }
            if (cbVacunaParvovirus.isChecked) {
                vacuna.add(cbVacunaParvovirus.text.toString())
            }
            if (cbVacunaDistemper.isChecked) {
                vacuna.add(cbVacunaDistemper.text.toString())
            }


            //Preparar bundle para enviar a la segunda pantalla
            val bundle = Bundle().apply {
                putString("key_nombre", nombres)
                putString("key_edad", edad)
                putString("key_mascota", mascota)
                putStringArrayList("key_vacuna", ArrayList(vacuna))
            }


            val intent = Intent(this, ResultadoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)


        }
    }
}
