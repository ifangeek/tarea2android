package com.ifangeek.lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_resultado.*

class ResultadoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)

        val bundle: Bundle? = intent.extras

        bundle?.let {
            val nombre = it.getString("key_nombre", "")
            val edad = it.getString("key_edad", "0")
            val mascota = it.getString("key_mascota", "")
            val vacunas = it.getStringArrayList("key_vacuna")

            // variable para concatenar los checkbox
            var cadena = ""

            tvResultadoNombre.text = nombre
            tvResultadoEdad.text = edad
            tvResultadoMascota.text = mascota
            if (vacunas.isNullOrEmpty().not()) {
                vacunas?.forEach { vacuna ->
                    cadena += "$vacuna\n"
                    Log.d(ResultadoActivity::class.java.simpleName, vacuna)
                }
            }
            tvResultadoVacuna.text = cadena

            when (mascota) {
                "perro" -> {
                    ivMascota.setBackgroundResource(R.drawable.perro)
                }
                "gato" -> {
                    ivMascota.setBackgroundResource(R.drawable.gato)
                }
                else -> {
                    ivMascota.setBackgroundResource(R.drawable.conejo)
                }
            }
        }
    }
}