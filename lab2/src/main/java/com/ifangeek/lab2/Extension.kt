package com.ifangeek.lab2

import android.content.Context
import android.widget.Toast

fun Context.Toast(mensaje: String) {
    Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
}